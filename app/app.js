import data from './data.json';
import Student from './student.js';
import Rainbow from 'rainbowvis.js';
import utils from './utils.js';
import $ from 'jquery';
import _ from 'lodash';

class App {
  constructor() {
    this.nbCol = 20;
    this.nbRow = 20;
    this.studentSize = 32;
    this.width;
    this.height;

    this.data = data;

    this.students = [];
    this.resultFilter1 = [];
    this.resultFilter2 = [];
    this.resultFiltered = [];

    this.courses = [];
    this.birthYears = [];
    this.locationCountries = [];
    this.counties = [];
    this.diplomaGraduation = [];
    this.experiencies = [];

    this.filters = [];

    //liste des filtres pertinent et leurs propriétés
    this.filterList = {
      course: {
        choices: {name:this.courses},
        color: ['#43c3ff','#432fff', '#8d30ff', '#df1eff'],
        name: 'Formation'
      },
      birthYear: {
        choices: {name:this.birthYears},
        color: ['#ffdb35','#ff947d', '#dd227a', '#562293'],
        name: 'Année de naissance'
      },
      locationCountry: {
        choices: {name:this.locationCountries},
        color: ['#3c43ed','#3c7ed9', '#4eddd9', '#70dd90'],
        name: 'Nationalité'
      },
      county: {
        choices: {name:this.counties},
        color: ['#1a884d','#3ee581', '#ffe631', '#ff9131'],
        name: 'Département'
      },
      diplomaGraduation: {
        choices: {name:this.diplomaGraduation},
        color: ['#ffe6c3','#ffb839', '#ff2a49', '#a2244c'],
        name: 'Niveau de diplome'
      },
      experience: {
        choices: {name:this.experienciescolor},
        color: ['#5cf4a1','#25d861', '#00505c', '#2a64c2'],
        name: 'Expérience'
      }
    };

    //lib rainbowjs pour les dégradés
    this.rainbow = new Rainbow();

    //créations des étudiants
    for (let i in this.data) {
      data[i].id = 's'+i;
      let student = new Student(data[i]);
      student.append();
      this.students.push(student);
    }

    //taille du main
    $('main').css('width', this.studentSize * this.nbCol);
    this.width = $('main').width();
    this.height = $('main').width();

    //filtre de départ obtenu en get dans l'url
    this.filters = {
      filter1: utils.getParameterByName('filter1'),
      value1: [],
      filter2: utils.getParameterByName('filter2'),
      value2: [],
    }

    //ajout des évènements relatif au menu (ouvrture/fermeture)
    $('.menu.top > div').on('click', (event) => {
      let $target = $(event.target);
      this.filters.filter1 = $($target).attr('class');
      $('.menu.top').removeClass('open');
      this.filters.value1 = [];
      this.updateFilter();
      this.updateSelect();
    });
    $('.menu.bottom > div').on('click', (event) => {
      let $target = $(event.target);
      this.filters.filter2 = $($target).attr('class');
      $('.menu.bottom').removeClass('open');
      this.filters.value2 = [];
      this.updateFilter();
      this.updateSelect();
    });
    $('#form-filter1 .title').on('click', function() {
      $('.menu.top').toggleClass('open');
    });
    $('#form-filter2 .title').on('click', function() {
      $('.menu.bottom').toggleClass('open');
    });

    this.initFilterList();
    if (this.filters.filter1 != '' && this.filters.filter2 != '') {
      this.updateFilter();
      this.updateSelect();
    }
  };

  //vue principal du plateau représentant les étudiants
  updateView() {
    let nbCol = this.nbCol;
    let nbRow = this.nbRow;

    let filter1 = this.filters.filter1;
    let value1 = this.filters.value1;
    let filter2 = this.filters.filter2;
    let value2 = this.filters.value2;

    let students = this.students;
    let resultFilter1 = this.resultFilter1;
    let resultFilter2 = this.resultFilter2;
    let resultFiltered = this.resultFiltered;
    let sqrtFilter1 = _.ceil(Math.sqrt(resultFilter1.length));

    let filterList = this.filterList;
    let sqrtFilterLength;
    let maxStudents;

    let map = [];
    let mapNbColList = [];
    let mapWidth = [];
    let index = 0;

    let rainbow = this.rainbow;
    if (filter2 != '') {
      rainbow.setSpectrum(filterList[filter2]['color'][0], filterList[filter2]['color'][1], filterList[filter2]['color'][2], filterList[filter2]['color'][3]);
      rainbow.setNumberRange(0, filterList[filter2]['choices']['name'].length - 1);
    }

    //reset
    for (let i in students) {
      if (resultFiltered.length == 0 && value1.length == 0 && value2.length == 0)
        students[i].filtered = true;
      else
        students[i].filtered = false;
    }

    // filtre1 + filter2
    for (let i in resultFiltered) {
      resultFiltered[i].filtered = true;
    }

    //radient filter2
    sqrtFilterLength = _.ceil(Math.sqrt(filterList[filter2]['choices']['name'].length));
    maxStudents = ((this.width / this.studentSize) / sqrtFilterLength) * nbRow;
    for (var i = 0; i < sqrtFilterLength; i++) {
      map[i] = [];
      mapWidth[i] = _.floor(nbCol / sqrtFilterLength) * this.studentSize;
      if (i == sqrtFilterLength - 1) {
        mapWidth[i] = this.width - (map.length - 1) * _.floor(nbCol / sqrtFilterLength) * this.studentSize;
      }
    }

    //rangement des étudiants en bloc de couleur par rapport au filtre2
    for (let f in filterList[filter2]['choices']['name']) {
      let color = '#'+rainbow.colourAt(f);
      for (let j in students) {
        if (students[j][filter2] == filterList[filter2]['choices']['name'][f]) {
          students[j].color = color;
          if (map[index].length / (mapWidth[index] / this.studentSize) < nbRow) {
            map[index].push(students[j]);
          } else {
            do {
              if (index < sqrtFilterLength - 1)
                  index++;
               else
                 index = 0;
            } while (map[index].length / (mapWidth[index] / this.studentSize) >= nbRow);
            map[index].push(students[j]);
          }
        }
      }
      mapNbColList = [];
      for (let m in map) {
        mapNbColList.push(_.ceil(map[m].length * this.studentSize / mapWidth[m]));
      }
      index = _.indexOf(mapNbColList,_.min(mapNbColList));
    }

    //réarengement visuel des étudiants sur le plateau
    for (let m in map) {
      if (m != map.length - 1) {
        $('section.container').append('<div id="m'+m+'" class="filter"></div>');
        $("#m"+m).css('width', mapWidth[m]);
      } else {
        $('section.container').append('<div id="m'+m+'" class="filter"></div>');
        $("#m"+m).css('width', mapWidth[m]);
      }
      for (let w in map[m]) {
        map[m][w].moveTo("#m"+m);
      }
    }
    $('section .filter').each(function() {
      if ($(this).html() == '') {
        $(this).remove();
      }
    });
  };

  //met à jour la liste des filtres
  updateSelect() {
    let filter1 = this.filters.filter1;
    let value1 = this.filters.value1;
    let filter2 = this.filters.filter2;
    let value2 = this.filters.value2;

    let filterList = this.filterList;

    let rainbow = this.rainbow;
    if (filter1 != '') {
      rainbow.setSpectrum(filterList[filter1]['color'][0], filterList[filter1]['color'][1], filterList[filter1]['color'][2], filterList[filter1]['color'][3]);
      rainbow.setNumberRange(0, filterList[filter1]['choices']['name'].length - 1);

      $('#form-filter1 .select').html('');
      $('#form-filter1 .title span').html(filterList[filter1]['name']);
      for (let i in filterList[filter1]['choices']['name']) {
        let color = '#'+rainbow.colourAt(i);
        if (filterList[filter1]['choices']['name'][i] !== "") {
          $('#form-filter1 .select').append('<div class="options">' +
            filterList[filter1]['choices']['name'][i] +
            '<div class="number">' +
              filterList[filter1]['choices']['number'][i]+
            '</div>' +
          '</div>');
          $('#form-filter1 .select .options').last().attr('data-filter-name',filterList[filter1]['choices']['name'][i]);
          $('#form-filter1 .select .options').last().css('color',color);
          $('#form-filter1 .select .options').last().css('border-color',color);
          for (let j in value1) {
            if(filterList[filter1]['choices']['name'][i] == value1[j]) {
              $('#form-filter1 .select .options').last().addClass('active');
            }
          }
        }
      }
      $('#form-filter1 .bar').css('background-color', filterList[filter1]['color'][0]);
    }

    if (filter2 != '') {
      rainbow.setSpectrum(filterList[filter2]['color'][0], filterList[filter2]['color'][1], filterList[filter2]['color'][2], filterList[filter2]['color'][3]);
      rainbow.setNumberRange(0, filterList[filter2]['choices']['name'].length - 1);

      $('#form-filter2 .select').html('');
      $('#form-filter2 .title span').html(filterList[filter2]['name']);
      for (let i in filterList[filter2]['choices']['name']) {
        let color = '#'+rainbow.colourAt(i);
        if (filterList[filter2]['choices']['name'][i] !== "") {
          $('#form-filter2 .select').append('<div class="options">' +
            filterList[filter2]['choices']['name'][i] +
            '<div class="number">' +
              filterList[filter2]['choices']['number'][i]+
            '</div>' +
          '</div>');
          $('#form-filter2 .select .options').last().attr('data-filter-name',filterList[filter2]['choices']['name'][i]);
          $('#form-filter2 .select .options').last().css('color',color);
          $('#form-filter2 .select .options').last().css('border-color',color);
          for (let j in value2) {
            if(filterList[filter2]['choices']['name'][i] == value2[j]) {
              $('#form-filter2 .select .options').last().addClass('active');
            }
          }
        }
      }
      $('#form-filter2 .bar').css('background-color', filterList[filter2]['color'][0]);
    }

    this.updateSelectEvent();
  };

  //ajout des évènements sur les options des filtres
  updateSelectEvent() {
    $('#form-filter1 .select .options').on('click', (event) => {
      let $target = $(event.target);
      if ($target.hasClass('active')) {
        _.pull(this.filters['value1'], $target.attr('data-filter-name'));
        $target.removeClass('active');
      } else {
        this.filters['value1'].push($target.attr('data-filter-name'));
        $target.addClass('active');
      }
      this.updateFilter();
    });
    $('#form-filter2 .select .options').on('click', (event) => {
      let $target = $(event.target);
      if ($target.hasClass('active')) {
        _.pull(this.filters['value2'], $target.attr('data-filter-name'));
        $target.removeClass('active');
      } else {
        this.filters['value2'].push($target.attr('data-filter-name'));
        $target.addClass('active');
      }
      this.updateFilter();
    });
  };

  //mise à jour des résultats obtenu selon les différents filtres
  updateFilter() {
    let filter1 = this.filters.filter1;
    let value1 = this.filters.value1;
    let filter2 = this.filters.filter2;
    let value2 = this.filters.value2;

    //reset
    this.resultFilter1 = [];
    this.resultFilter2 = [];
    this.resultFiltered = [];

    let students = this.students;
    let resultFilter1 = this.resultFilter1;
    let resultFilter2 = this.resultFilter2;
    let resultFiltered = this.resultFiltered;


    for (let i in students) {
      for (let j in value1) {
        if(students[i][filter1] == value1[j]) {
          resultFilter1.push(students[i]);
        }
      }
      for (let w in value2) {
        if(students[i][filter2] == value2[w]) {
          resultFilter2.push(students[i]);
        }
      }
    }
    for (let j in resultFilter1) {
      for (let w in resultFilter2) {
        if(resultFilter1[j] === resultFilter2[w]) {
          resultFiltered.push(resultFilter1[j]);
        }
      }
    }

    if (filter1 != '' && filter2 != '') {
      this.updateView();
    }
  };

  //initialise les différentes options des différents filtres
  initFilterList() {
    let students = this.students;

    let courseName = []
    let birthYearName = []
    let locationCountryName = []
    let countyName = []
    let diplomaGraduationName = []
    let experienceName = []
    let courseNumber= []
    let birthYearNumber= []
    let locationCountryNumber= []
    let countyNumber= []
    let diplomaGraduationNumber= []
    let experienceNumber= []

    for (let i in this.data) {
      if (_.indexOf(courseName,data[i]['course']) == -1) {
        courseName.push(data[i]['course']);
        courseNumber.push(0);
      }
      if (_.indexOf(birthYearName,data[i]['birth_year']) == -1) {
        birthYearName.push(data[i]['birth_year'])
        birthYearNumber.push(0);
      }
      if (_.indexOf(locationCountryName,data[i]['location_country']) == -1) {
        locationCountryName.push(data[i]['location_country']);
        locationCountryNumber.push(0);
      }
      if (_.indexOf(countyName,data[i]['county']) == -1) {
        countyName.push(data[i]['county']);
        countyNumber.push(0);
      }
      if (_.indexOf(diplomaGraduationName,data[i]['diploma_graduation']) == -1) {
        diplomaGraduationName.push(data[i]['diploma_graduation']);
        diplomaGraduationNumber.push(0);
      }
      if (_.indexOf(experienceName,data[i]['experience']) == -1) {
        experienceName.push(data[i]['experience']);
        experienceNumber.push(0);
      }
    }

    courseName = _.sortBy(courseName);
    birthYearName = _.sortBy(birthYearName);
    locationCountryName = _.sortBy(locationCountryName);
    countyName = _.sortBy(countyName);
    diplomaGraduationName = _.sortBy(diplomaGraduationName);
    experienceName = _.sortBy(experienceName);

    for (let i in students) {
      for (let j in courseName) {
        if (students[i].course == courseName[j]) {
          courseNumber[j]++
        }
      }
      for (let j in birthYearName) {
        if (students[i].birthYear == birthYearName[j]) {
          birthYearNumber[j]++
        }
      }
      for (let j in locationCountryName) {
        if (students[i].locationCountry == locationCountryName[j]) {
          locationCountryNumber[j]++
        }
      }
      for (let j in countyName) {
        if (students[i].county == countyName[j]) {
          countyNumber[j]++
        }
      }
      for (let j in diplomaGraduationName) {
        if (students[i].diplomaGraduation == diplomaGraduationName[j]) {
          diplomaGraduationNumber[j]++
        }
      }
      for (let j in experienceName) {
        if (students[i].experience == experienceName[j]) {
          experienceNumber[j]++
        }
      }
    }

    this.filterList['course']['choices']['name'] = courseName;
    this.filterList['birthYear']['choices']['name'] = birthYearName;
    this.filterList['locationCountry']['choices']['name'] = locationCountryName;
    this.filterList['county']['choices']['name'] = countyName;
    this.filterList['diplomaGraduation']['choices']['name'] = diplomaGraduationName;
    this.filterList['experience']['choices']['name'] = experienceName;
    this.filterList['course']['choices']['number'] = courseNumber;
    this.filterList['birthYear']['choices']['number'] = birthYearNumber;
    this.filterList['locationCountry']['choices']['number'] = locationCountryNumber;
    this.filterList['county']['choices']['number'] = countyNumber;
    this.filterList['diplomaGraduation']['choices']['number'] = diplomaGraduationNumber;
    this.filterList['experience']['choices']['number'] = experienceNumber;
  };

};

export default App;
