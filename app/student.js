import $ from 'jquery';

class Student {
  constructor(data) {
    this.id = data['id'];
    this.course = data['course'];
    this.birthYear = data['birth_year'];
    this.locationCountry = data['location_country'];
    this.county = data['county'];
    this.countyName = data['county_name'];
    this.countyNumber = data['county_number'];
    this.locationTown = data['location_town'];
    this.diplomaGraduation = data['diploma_graduation'];
    this.diplomaName = data['diploma_name'];
    this.experience = data['experience'];
    this.choice1 = data['choice_1'];
    this.choice2 = data['choice_2'];
    this.choice3 = data['choice_3'];
    this.source1 = data['source_1'];
    this.source2 = data['source_2'];
    this.source3 = data['source_3'];
    this.color = "rgb(230,230,230)";
    this.filtered = true;
  };

  //créer le noeud dans le dom
  append() {
    $('section.container').append('<div id="'+this.id+'" class="student"></div>');
    $('#'+this.id).css('background-color', this.color);
    if (this.filtered)
      $('#'+this.id).addClass('active');
    else
      $('#'+this.id).removeClass('active');

    this.hover();
  };

  //déplace le noeud dans le dom
  moveTo(el) {
    $('#'+this.id).remove();
    $(el).append('<div id="'+this.id+'" class="student"></div>');
    $('#'+this.id).css('background-color', this.color);
    if (this.filtered)
      $('#'+this.id).addClass('active');
    else
      $('#'+this.id).removeClass('active');

    this.hover();
  };

  //fait apparaitre un tooltip au survol
  hover() {
    $('#'+this.id).mouseenter((event) => {
      let $target = $(event.target);
      $($target).append('<div class="tooltip">' +
        '<div class="fakeStudent"></div>' +
        '<div class="infos">' +
          '<p>' + this.course + '</p>' +
          '<p>' + this.birthYear + '</p>' +
          '<p>' + this.locationCountry + '</p>' +
          '<p>' + this.county + '</p>' +
          '<p>' + this.diplomaGraduation + '</p>' +
          '<p>' + this.experience + ' ans</p>' +
        '</div>' +
        '<div class="properties">' +
          '<p>Formation</p>' +
          '<p>Année de naissance</p>' +
          '<p>Nationalité</p>' +
          '<p>Département</p>' +
          '<p>Niveau de diplome</p>' +
          '<p>Expérience' + '</p>' +
        '</div>' +
      '</div>');
      $('#'+this.id + ' .tooltip .properties').css('color', this.color);
      $('#'+this.id + ' .tooltip .infos').css('background-color', this.color);
      $('#'+this.id + ' .tooltip .fakeStudent').css('background-color', this.color);
    });
    $('#'+this.id).mouseleave((event) => {
     $('.student').find('.tooltip').remove();
    });
  };
};

export default Student;
